
#include <avr/io.h>
#include "UART.h"
#define F_CPU 8000000UL
unsigned char data_rx = 0;
void Init_UART()
{
	UCSRB=(1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	UCSRC=(1<<UCSZ1)|(1<<UCSZ0);
	UBRRH=0x00;
	UBRRL=51;
}


void UART_TX_BYTE(char data)
{
	while((UCSRA&(1<<UDRE))==0);
	UDR=data;
}

void UART_TX_string(char word[])
{
	unsigned char i=0; // iterating variable
	unsigned char length = (unsigned char)strlen(word); // length of the word
	
	// sending the word
	for(i=0;i<length;i++)
	{
		UART_TX_BYTE(word[i]);
	}
}

void UART_TX_integer(int number)
{
	char num[10];
	sprintf(num,"%d",number); // function sprintf converts integer to string

	UART_TX_string(num);
}


char UART_RX_BYTE()
{
	while((UCSRA&(1<<RXC))==0);
	return UDR;

}
unsigned char UART_RX_BYTE_2()
{
	unsigned char temp ;
	temp = data_rx ;
	data_rx = 0 ;
	return temp ;
}
ISR(USART_RXC_vect)
{
	data_rx = UDR ;
}