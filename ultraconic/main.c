/*
 * ultraconic.c
 *
 * Created: 15/6/2016 7:46:45 AM
 * Author : Ibrahim
 */ 


#define F_CPU 16000000UL // crystal frequency

//FROM DATA SHEET:
//F_OUT = F_CPU / (2 * PRECSALER * (1 + OCR0_VALUE)),
//OCR0_VALUE = ((F_CPU/(2*PRECSALER*F_OUT))-1)
// HOWEVER, by scoping the signal on Oscilloscope it gets frequency of 20 Kh
// so the equation should be: OCR0_VALUE = ((F_CPU/(4*PRECSALER*F_OUT))-1)

//sender
#define PRECSALER 8
#define F_OUT 40000 // output frequency
#define OCR0_VALUE ((F_CPU/(4*PRECSALER*F_OUT))-1)// =((16000000/(4*8*40000))-1= 11.5
#define NUM_OF_40Kh 20

//decelerations
#define FALSE 0
#define TRUE 1
#define TRIGGER PB3
#define ECHO PD6

#define LED_R PC1
#define LED_M PC2


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "uart.h"

//global variables:
	//sender
volatile uint32_t pulse_count;
volatile uint32_t pulses_sent;	//bool
	//receiver
volatile uint32_t echo_received; //bool
volatile uint32_t echo_missed;  //bool
uint32_t counts;
volatile uint32_t distance;

//functions
void intial(void);
void sender(void);
void receive(void);
void disable(void);
void calculate(void);


int main(void)
{
	 Init_UART(); //to transmit result to PC

	DDRB |= (1<<TRIGGER); // select as output pin to use OC0 pin function
	DDRD &= ~(1<<ECHO);	 //select as input pin to use ICP1 pin function
	
	DDRC |= (1<<LED_R); //led is output
	DDRC |= (1<<LED_M); //led is output
	
	cli();
	
	while(1){
				
		intial();
		sender(); 
		receive();	
	}
}

void intial(){
	
	pulses_sent = FALSE;
	pulse_count = NUM_OF_40Kh;
	echo_received = FALSE;
	echo_missed   = FALSE;
}
//////////////////////////////////////////////end of intial


///generate n pulses of 40 Kh signal to drive transmitter:
void sender()
{
	
///timer/counter0 CTC mode:
	// WGM0[1:0]= 10, for CTC mode
	// COM0[1:0]= 01, to toggle OC0 on compare match // to generate square wave.
	// CS0[2:0] =010. for prescaler 8
	TIMSK |=(1<<OCIE0); //enable output compare interrupt
	TCCR0 =(1<<WGM01)|(1<<COM00)|(1<<CS01);
	
	sei(); // enable global interrupts
		
	/* wait for the pulses to go. */
	while(pulses_sent == FALSE)
	{
	}
               
	/* Make sure we don't pick up any cross talk between
		the transmitter and receiver. */
	_delay_us(100);        
	cli();
}

ISR(TIMER0_COMP_vect) // interrupt subroutine
{
	OCR0=(uint8_t)OCR0_VALUE; //put OCR value
	
	if (pulse_count == 0)
	{
		pulse_count = NUM_OF_40Kh;  //reinitialize value 
		TCCR0 = (1<<CS00); //disable timer by removing prescaller.
        pulses_sent = TRUE;

	} 
	else
	{
		OCR0=(uint8_t)OCR0_VALUE; //put OCR value
		pulse_count--;
	}
}
/////////////////////////////////////////////end of sender

void receive(){
	
	//using 16 bit timer/counter1:
	//start timer and save its value at interrupt.
	TCCR1B = (1<<ICES1) //set interrupt at rising edge
			| (1<<CS10)|(1<<CS11) //set prescaller at 64
			|(1<<ICNC1); //set noise canceler
	TCNT1 = 0;
	
	TIMSK = (1<<TICIE1)  //enable input capture interrupt
			|(1<<TOIE1); //enable Overflow Interrupt 
	
	sei();
	
    /* Wait for some echo activity. */
    while(echo_received == FALSE && echo_missed == FALSE)
    {
    }
	
	if (echo_received == TRUE)
	{
		disable();
		calculate();
        //_delay_ms(1000);/* Create a long delay before starting again.This is so we don't detect any double echos. */
		
		PORTC |= (1<<LED_R); //ON		   
		_delay_ms(50);
		PORTC &= ~(1<<LED_R); //OFF	
		
		return;
	} 
	
	if(echo_missed == TRUE){
		
		disable();
		UART_TX_BYTE('@');	//to know the echo is missed
		//_delay_us(200);
		
		PORTC |= (1<<LED_M); //ON
		_delay_ms(50);
		PORTC &= ~(1<<LED_M); //OFF
		
		return;
	}

}

//Echo received:
ISR(TIMER1_CAPT_vect){
	
	counts = ICR1;  //input capture register1
	echo_received = TRUE;

}

//Echo missed:
ISR(TIMER1_OVF_vect){
	//TCNT1 = 0;
	echo_missed = TRUE;
}

////////////////////////////////////////////end of receive
void disable(){
	TCNT0 = 0;
	TCCR0 = (1<<CS00); //disable timer by removing prescaller.
	
}
////////////////////////////////////////////end of disable

void calculate(){
	
	/* Speed of sound = 1cm every 30us
        Timer Prescaler = 64
        External Crystal = 16MHz
        Timer Updates Every (1/16MHz) * 64 = 4uS
               
        distance = (counts * 4) / 30  */
    distance = (counts*4) / 30;
	
	UART_TX_BYTE('#');
	UART_TX_integer(distance);
	UART_TX_BYTE('&');

}

////////////////////////////////////////////end of calculate
















