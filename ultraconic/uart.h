/*
 * UART.h
 *
 * Created: 10/6/2016 6:33:33 PM
 *  Author: Ibrahim
 */ 


#ifndef UART_H_
#define UART_H_

#include <avr/interrupt.h>

void Init_UART();
void UART_TX_BYTE(char data);
void UART_TX_string(char word[]);
void UART_TX_integer(int number);
char UART_RX_BYTE();
unsigned char UART_RX_BYTE_2() ;

#endif /* UART_H_ */