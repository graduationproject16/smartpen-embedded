# Smart Pen

Smart pen is a digital note taking system that enable user to take notes on regular paper
and see their notes transferred to their computer device where they can share using web sites system in real time.

# This represents the embedded software code for Smart Pen project.

* Code is created by Ibrahim Mahdy.
* The project is written in C for AVR microcontroller (ATMEGA 16) to drive Ulrasonic sensors. 
* The idea was to calculate the distance between a transmitter ultrasonic sensor and 2 pairs of Ultrasonic recievers. 
However, the represented code gets the distance between the transmitter and one receiver then transfer the result to a PC via Bluetooth module 
using UART Serial Communication Protocol.

* This was the final result we accomplished at the time of the project discussion.
The reason we did not finish the whole project by the deadline was due to the lack of resources as we had to buy an Oscilloscope and import the hardware components from China with our own money
that took a long time and resulted for us to phased out of Microsoft Imagine Cup and Made In Egypt competitions

* The Hardware circuit design was adopted from [3D Paint](http://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/s2012/wgm37_gc348/wgm37_gc348/index.html)

To check the project full documentation visit this [link](https://drive.google.com/file/d/1U_OQTZ40pwyhT3x178S9Y90uItywCBWm/view?usp=sharing) 





